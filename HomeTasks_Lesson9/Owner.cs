﻿using System;

namespace HomeTasks_Lesson9
{
    class Owner
    {
        private int age;
        private string name;

        public int Age { get { return age; } }
        public string Name { get { return name; } }

        public Owner(int age, string name)
        {
            this.name = name;
            this.age = age;
        }
    }
}
