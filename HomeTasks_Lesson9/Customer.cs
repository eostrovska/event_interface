﻿using System;

namespace HomeTasks_Lesson9
{
    class Customer : Owner
    {
        public Customer(int age, string name) : base(age, name) { }

        public void AcceptAsAGift()
        {
            Console.WriteLine("Accept as a gift");
        }

        public void Gift()
        {
            Console.WriteLine("Gift a present.");
        }
    }
}
