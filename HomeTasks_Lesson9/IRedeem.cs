﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeTasks_Lesson9
{
    interface IRedeem<in T>
    {
        void Redeem();
    }
}
