﻿using HomeTasks_Lesson9;
using System;

namespace Task1_Popshop
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Task 1: Create a class-generic PopShop. A PopShop implements two interfaces. One interface has a method of 'Pawnshop' and works on covariance." +
                               "The second interface has the method of 'Redeem' and works on contravariance.Pawnshop can take all type of electronics.");
            Console.WriteLine(new string('-', 60));

            Customer customer = new Customer(23, "Kate");
            IPawnshop<Owner> owner = new Popshop<Customer>(customer);

            Owner someOwner = new Customer(34, "Dak");
            IRedeem<Customer> debtor = new Popshop<Owner>(someOwner);
            
            Console.ReadKey();
        }
    }
}
