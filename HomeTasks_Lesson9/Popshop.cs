﻿using System;

namespace HomeTasks_Lesson9
{
    enum Electronics
    {
        Tablet,
        Phone,
        Player,
        TV,
        Console
    }

    class Popshop<T> : IPawnshop<T>, IRedeem<T>
    {
        private T argument;
        private float money;

        public float Money { get; set; }

        public Popshop(T argument)
        {
            this.argument = argument;
        }

        public void AdoptElectronics(Electronics electronicThing)
        {
            Console.WriteLine("Your {0} is costs some money", electronicThing);
        }

        public void Pawnshop()
        {
            Console.WriteLine("Pawshop");
            money++;
        }

        public void Redeem()
        {
            Console.WriteLine("Redeem.");
            money--;
        }
    }
}
