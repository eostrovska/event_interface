﻿using System;

namespace Task2_Events
{
    enum TypeOfLearning
    {
        InPatient,
        Correspondence
    }

    enum Gender
    {
        Male,
        Female
    }

    class Student
    {
        private string name;
        private byte age;
        private TypeOfLearning typeOfLearning;
        private string group;
        private Gender gender;

        public string Name { get { return name; } }
        public byte Age { get { return age; } }
        public TypeOfLearning TypeOfLerning { get { return typeOfLearning; } }
        public string Group { set; get; }
        public Gender Gender { get { return gender; } }

        public Student(string name, byte age, TypeOfLearning typeOfLearning, string group, Gender gender)
        {
            this.age = age;
            this.name = name;
            this.typeOfLearning = typeOfLearning;
            this.group = group;
            this.gender = gender;
        }

        public void Read(string book)
        {
            Console.WriteLine("Student are reading {0} now.", book);
        }

        public void SummerStudentSubscribe(University university)
        {
            university.SummerSession += new University.EventDelegate(SummerSessionStudentHandler);
        }

        public void WinterStudentSubscribe(University university)
        {
            university.WinterSession += new University.EventDelegate(WinterSessionStudentHandler);
        }

        static public void SummerSessionStudentHandler()
        {
            Console.WriteLine("Student prepare to the summer sea holiday.");
        }

        static public void WinterSessionStudentHandler()
        {
            Console.WriteLine("Student prepare to the Winter holiday and NY.");
        }
    }
}
