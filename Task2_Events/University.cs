﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2_Events
{


    class University
    {
        private string name;
        private short numberOfChair;
        public delegate void EventDelegate();

        public event EventDelegate SummerSession;
        public event EventDelegate WinterSession;

        public string Name { get { return name; } }
        public short NumberOfChair { get { return numberOfChair; } }

        public University(string name, short numberOfChair)
        {
            this.name = name;
            this.numberOfChair = numberOfChair;
        }

        public void StartEvents()
        {
            if (SummerSession != null)
            {
                SummerSession.Invoke();
            }

            if (WinterSession != null)
            {
                WinterSession.Invoke();
            }
        }
    }
}
