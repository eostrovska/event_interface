﻿using System;

namespace Task2_Events
{
    enum Department
    {
        Mathematics,
        Philosophy,
        Management,
        ComputerScience
    }

    class Professor
    {
        private string name;
        private byte experienceYears;
        private string subject;
        private Department department;

        public string Name { get { return name; } }
        public byte ExpirienceYears { get { return experienceYears; } }
        public string Subject { set; get; }
        public Department Department { get { return department; } }

        public Professor(string name, byte experienceYears, string subject, Department department)
        {
            this.name = name;
            this.experienceYears = experienceYears;
            this.subject = subject;
            this.department = department;
        }

        public void TeachStudents(string subject)
        {
            if (subject == Subject)
            {
                Console.WriteLine("Good professor.");
            }
            else
            {
                Console.WriteLine("");
            }
        }

        public void SummerProfessorSubscribe(University university)
        {
            university.SummerSession += new University.EventDelegate(SummerSessionProfessorHandler);
        }

        public void WinterProfessorSubscribe(University university)
        {
            university.WinterSession += new University.EventDelegate(WinterSessionProfessorHandler);

        }

        static public void SummerSessionProfessorHandler()
        {
            Console.WriteLine("Professor prepare to the summer session and summer holiday.");
        }

        static public void WinterSessionProfessorHandler()
        {
            Console.WriteLine("Professor prepare to the winter session and New Year Holiday.");
        }
    }
}
