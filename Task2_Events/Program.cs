﻿using System;

namespace Task2_Events
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Task 2: Create the Univistet class. In the university class there are two events, 'Summer session' and 'Winter session'." +
                              "Create class Student and class Professor. Both the student and the professor subscribe for the event session and do something.");
            Console.WriteLine(new string('-', 60));

            Professor professorOfComputerScience = new Professor("Nazar", 17, "Networks", Department.ComputerScience);
            University university = new University("KUCAS", 5);
            Student student = new Student("Kate", 23, TypeOfLearning.InPatient, "T56", Gender.Female);

            professorOfComputerScience.SummerProfessorSubscribe(university);
            professorOfComputerScience.WinterProfessorSubscribe(university);
            student.SummerStudentSubscribe(university);
            student.WinterStudentSubscribe(university);
            university.StartEvents();

            Console.ReadKey();
        }
    }
}
