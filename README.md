### What is this repository for? ###

* Task 1: Create a class-generic PopShop. A PopShop implements two interfaces. One interface has a method of 'Pawnshop' and works on covariance.
  The second interface has the method of 'Redeem' and works on contravariance.Pawnshop can take all type of electronics.
* Task 2: Create the Univistet class. In the university class there are two events, 'Summer session' and 'Winter session'.
          Create class Student and class Professor. Both the student and the professor subscribe for the event session and do something.